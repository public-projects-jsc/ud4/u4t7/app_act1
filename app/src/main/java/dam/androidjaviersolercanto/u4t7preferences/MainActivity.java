package dam.androidjaviersolercanto.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private LinearLayout layout;
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private Spinner spinnerBackground;
    private EditText etScore;
    private Button btQuit;
    private RadioGroup rgDifficulty;
    private RadioButton rbEasy;
    private RadioButton rbNormal;
    private RadioButton rbHard;
    private RadioButton rbVeryHard;
    private RadioButton rbExpert;
    private CheckBox cbSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        layout = findViewById(R.id.layout);

        etPlayerName = findViewById(R.id.etPlayerName);

        // Level spinner, set adapter from string-array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinnerBackground = findViewById(R.id.spinnerBackground);
        spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.backgrounds, android.R.layout.simple_spinner_item);
        spinnerBackground.setAdapter(spinnerAdapter);

        rgDifficulty = findViewById(R.id.rgDifficulty);

        rbEasy = findViewById(R.id.rbEasy);
        rbNormal = findViewById(R.id.rbNormal);
        rbHard = findViewById(R.id.rbHard);
        rbVeryHard = findViewById(R.id.rbVeryHard);
        rbExpert = findViewById(R.id.rbExpert);

        cbSound = findViewById(R.id.cbSound);
    }

    // save activity data to preferences file
    @Override
    protected void onPause() {
        super.onPause();

        // get preference file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // save UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName",etPlayerName.getText().toString());
        editor.putInt("Level",spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score",Integer.parseInt(etScore.getText().toString()));
        editor.putInt("Background",spinnerBackground.getSelectedItemPosition());
        editor.putBoolean("Sound",cbSound.isChecked());
        editor.putInt("Difficulty",rgDifficulty.getCheckedRadioButtonId());

        editor.commit();
    }

    // read activity data from preferences file

    @Override
    protected void onResume() {
        super.onResume();

        // get preference file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS,MODE_PRIVATE);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName","uknonwn"));
        spinnerLevel.setSelection(myPreferences.getInt("Level",0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score",0)));
        spinnerBackground.setSelection(myPreferences.getInt("Background",0));
        cbSound.setChecked(myPreferences.getBoolean("Sound",true));

        switch (myPreferences.getInt("Difficulty",0)) {
            case R.id.rbEasy: rbEasy.setChecked(true); break;
            case R.id.rbNormal: rbNormal.setChecked(true); break;
            case R.id.rbHard: rbHard.setChecked(true); break;
            case R.id.rbVeryHard: rbVeryHard.setChecked(true); break;
            case R.id.rbExpert: rbExpert.setChecked(true); break;
        }

        changeBackground();
    }

    private void changeBackground() {
        switch (spinnerBackground.getSelectedItemPosition()) {
            case 0: layout.setBackgroundColor(getResources().getColor(R.color.red)); break;
            case 1: layout.setBackgroundColor(getResources().getColor(R.color.blue)); break;
            case 2: layout.setBackgroundColor(getResources().getColor(R.color.green)); break;
            case 3: layout.setBackgroundColor(getResources().getColor(R.color.orange)); break;
            case 4: layout.setBackgroundColor(getResources().getColor(R.color.white)); break;
        }
    }
}
